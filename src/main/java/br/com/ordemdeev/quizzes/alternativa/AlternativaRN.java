package br.com.ordemdeev.quizzes.alternativa;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.grupo.GrupoRN;
import br.com.ordemdeev.quizzes.pergunta.Pergunta;
import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.usuario.UsuarioRN;
import br.com.ordemdeev.quizzes.util.DAOFactory;

public class AlternativaRN {
	
	private AlternativaDAO alternativaDAO;
	
	public AlternativaRN()
	{
		this.alternativaDAO = DAOFactory.criarAlternativaDAO();
	}
	
	public List<Alternativa> listar(Pergunta pergunta)
	{
		return this.alternativaDAO.listar(pergunta);
	}
	
	public Alternativa carregar(Integer codigo)
	{
		return this.alternativaDAO.carregar(new Alternativa(), codigo);
	}
	
	public void salvar(Alternativa alternativa)
	{
		
		this.alternativaDAO.salvar(alternativa);

	}
	
	public void excluir(Alternativa alternativa)
	{
		UsuarioRN usuarioRN = new UsuarioRN();
		List<Usuario> usuarios = usuarioRN.listar();
		
		
		for(int i=0;i<usuarios.size(); i++)
		{
			Iterator<Alternativa> respostasIt = usuarios.get(i).getRespostas().iterator();
			while(respostasIt.hasNext())
			{
				Alternativa respostaIt = respostasIt.next();
				if(respostaIt.getCodigo().equals(alternativa.getCodigo()))
				{
					respostasIt.remove();
					usuarioRN.salvar(usuarios.get(i));
					break;
				}
			}
		}
		
		alternativa = this.carregar(alternativa.getCodigo());
		this.alternativaDAO.excluir(alternativa);
	}
	
	public List<Alternativa> buscarAlternativasUsuario(Usuario usuario, Grupo grupo)
	{
		List<Alternativa> alternativas = new ArrayList<>();
		
		GrupoRN grupoRN = new GrupoRN();
		grupo = grupoRN.carregar(grupo.getCodigo());
		
		Iterator<Pergunta> perguntasIterator = grupo.getPerguntas().iterator();
		
		//esse while é para percorrer todas as perguntas
		while(perguntasIterator.hasNext())
		{
			Pergunta perguntaAtualIt = perguntasIterator.next();
			Iterator<Alternativa> alternativaPerguntaIterator = perguntaAtualIt.getAlternativas().iterator();
			//esse while é para percorrer todas as alternativas das perguntas
			while(alternativaPerguntaIterator.hasNext())
			{
				Alternativa alternativaAtualIt = alternativaPerguntaIterator.next();
				Iterator<Alternativa> respostaUsuarioIterator = usuario.getRespostas().iterator();
				//esse while é para percorrer as respostas do usuario
				while(respostaUsuarioIterator.hasNext())
				{
					
					Alternativa it = respostaUsuarioIterator.next();
					if(it.getCodigo().equals(alternativaAtualIt.getCodigo()))
					{
						alternativas.add(it);
						break;
					}
					
				}
				
			}
			
		}
		
		
		return alternativas;
	}

}
