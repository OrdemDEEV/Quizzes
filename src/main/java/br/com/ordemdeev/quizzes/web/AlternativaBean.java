package br.com.ordemdeev.quizzes.web;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.ordemdeev.quizzes.alternativa.Alternativa;
import br.com.ordemdeev.quizzes.alternativa.AlternativaRN;
import br.com.ordemdeev.quizzes.pergunta.Pergunta;

@ManagedBean(name="alternativaBean")
@RequestScoped
public class AlternativaBean {
	
	private Pergunta pergunta;
	private Alternativa alternativaCadastro = new Alternativa();
	List<Alternativa> lista = null;
	
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	
	public String salvar()
	{
		String mensagemValidacao = new String();
		
		if(alternativaCadastro.getDescricao().equals(""))
		{
			mensagemValidacao = "Alternativa inválida\n";
		}
		
		
		if(!mensagemValidacao.equals(""))
		{
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage(mensagemValidacao);
			context.addMessage(null, facesMessage);
			return null;
		}
		
		if(alternativaCadastro.getCodigo() == null || alternativaCadastro.getCodigo() == 0)
		{
			alternativaCadastro.setPergunta(this.getContextoBean().getPerguntaAtiva());
		}
		
		AlternativaRN alternativaRN = new AlternativaRN();
		alternativaRN.salvar(alternativaCadastro);
		
		alternativaCadastro = null;
		lista = null;
		return null;
	}
	
	public String excluir()
	{
	    AlternativaRN alternativaRN = new AlternativaRN();
	    alternativaRN.excluir(alternativaCadastro);
	    
	    alternativaCadastro = null;
	    lista = null;
	    return null;
	}
	

	public Pergunta getPergunta() {
		
		if(pergunta == null)
		{
			pergunta = contextoBean.getPerguntaAtiva();
		}
		
		return pergunta;
	}
	
	public List<Alternativa> getLista() {
		
		if(lista == null)
		{
			AlternativaRN alternativaRN = new AlternativaRN();
			lista = alternativaRN.listar(this.contextoBean.getPerguntaAtiva());
		}
		
		return lista;
	}

	public void setLista(List<Alternativa> lista) {
		this.lista = lista;
	}
	

	public Alternativa getAlternativaCadastro() {
		return alternativaCadastro;
	}

	public void setAlternativaCadastro(Alternativa alternativaCadastro) {
		this.alternativaCadastro = alternativaCadastro;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}

	public ContextoBean getContextoBean() {
		return contextoBean;
	}

	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}
	
	
	
	
	
	

}
