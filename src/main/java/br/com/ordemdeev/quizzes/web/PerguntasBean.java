package br.com.ordemdeev.quizzes.web;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.pergunta.Pergunta;
import br.com.ordemdeev.quizzes.pergunta.PerguntaRN;

@ManagedBean(name = "PerguntasBean")
@RequestScoped
public class PerguntasBean {
	
	private Grupo grupo;
	private Pergunta perguntaCadastro = new Pergunta();
	private List<Pergunta> lista = null;
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	public String salvar()
	{
		if(perguntaCadastro.getDescricao().equals(""))
		{
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Questão inválida");
			context.addMessage(null, facesMessage);
			return null;
		}
		
		perguntaCadastro.setGrupo(this.getGrupo());
		PerguntaRN perguntaRN = new PerguntaRN();
		perguntaRN.salvar(perguntaCadastro);
		
		lista = null;
		perguntaCadastro = null;
		return null;
		
	}
	
	public String abrirCadastroDeAlternativas()
	{
		this.contextoBean.setPerguntaAtiva(perguntaCadastro);
		return "alternativas";
	}
	
	public String excluir()
	{
		PerguntaRN perguntaRN = new PerguntaRN();
		perguntaRN.excluir(perguntaCadastro);
		
		perguntaCadastro = null;
		lista = null;
		return null;
	}
	
	public Grupo getGrupo() {
		
		if(this.grupo == null)
		{
			this.grupo = contextoBean.getGrupoAtivo();
		}
		
		return grupo;
	}
	
	public List<Pergunta> getLista() {
		
		if(lista == null)
		{
			PerguntaRN perguntaRN = new PerguntaRN();
			lista = perguntaRN.listar(this.getGrupo());
		}
		
		return lista;
	}
	
	public void setLista(List<Pergunta> lista) {
		this.lista = lista;
	}
	
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}


	public Pergunta getPerguntaCadastro() {
		return perguntaCadastro;
	}
	
	
	public void setPerguntaCadastro(Pergunta perguntaCadastro) {
		this.perguntaCadastro = perguntaCadastro;
	}


	public ContextoBean getContextoBean() {
		return contextoBean;
	}

	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}
	
	
	
	

}
