package br.com.ordemdeev.quizzes.web;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import br.com.ordemdeev.quizzes.alternativa.Alternativa;
import br.com.ordemdeev.quizzes.alternativa.AlternativaRN;
import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.pergunta.Pergunta;
import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.usuario.UsuarioRN;

@ManagedBean(name = "responderBean")
@RequestScoped
public class ResponderBean {
	
	private Grupo grupo = null;
	private Pergunta pergunta = null;
	private Alternativa alternativa = new Alternativa();
	private List<Alternativa> alternativas;
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	
	public String salvareCarregarProxima()
	{
		
		
		if(alternativa == null)
		{
			return null;
		}
		
		if(alternativa.getCodigo() != null)
		{
			Usuario usuario = contextoBean.getUsuarioLogado();
			usuario.getRespostas().add(alternativa);
			
			UsuarioRN usuarioRN = new UsuarioRN();
			usuarioRN.salvar(usuario);
		}
		
		
		contextoBean.setNumeroPergunta(contextoBean.getNumeroPergunta()+1);
		if(contextoBean.getNumeroPergunta() >= contextoBean.getPerguntasResponder().size())
		{
			if(alternativa.getCodigo() == null && contextoBean.getNumeroPergunta() == 1)
			{
				contextoBean.setNumeroPergunta(0);
				contextoBean.setPerguntasResponder(null);
				return "home";
			}
			
			contextoBean.setNumeroPergunta(0);
			contextoBean.setPerguntasResponder(null);
			return "pontuacaoResposta";
		}
		
		alternativa = null;
		alternativas = null;
		pergunta = null;
		return null;
	}
	
	

	public Grupo getGrupo() {
		
		if(grupo == null)
		{
			grupo = this.contextoBean.getGrupoAtivo();
		}
		
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Alternativa getAlternativa() {
		return alternativa;
	}

	public void setAlternativa(Alternativa alternativa) {
		this.alternativa = alternativa;
	}

	public ContextoBean getContextoBean() {
		return contextoBean;
	}

	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}

	public List<Alternativa> getAlternativas() {
		
		if(alternativas == null)
		{
			AlternativaRN alternativaRN = new AlternativaRN();
			alternativas = alternativaRN.listar(this.contextoBean.getPerguntaAtiva());
			
			UsuarioRN usuarioRN = new UsuarioRN();
			usuarioRN.removerPerguntasRespondidas(contextoBean.getUsuarioLogado(), alternativas);
			
		}
		
		return alternativas;
	}

	public void setAlternativas(List<Alternativa> alternativas) {
		this.alternativas = alternativas;
	}

	public Pergunta getPergunta() {
		
		if(pergunta == null)
		{
			pergunta = contextoBean.retornaPerguntaLista();
		}
		
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}
	
	
	

}
