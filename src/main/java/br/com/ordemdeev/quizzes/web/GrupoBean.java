package br.com.ordemdeev.quizzes.web;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.grupo.GrupoRN;
import br.com.ordemdeev.quizzes.usuario.Usuario;

@ManagedBean(name="grupoBean")
@RequestScoped
public class GrupoBean {
	
	Grupo grupoCadastro = new Grupo();
	List<Grupo> lista = null;
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	
	
	public String salvar()
	{
		
		if(this.grupoCadastro.getTitulo() == null || this.grupoCadastro.getTitulo() == "")
		{
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Titulo inválido");
			context.addMessage(null, facesMessage);
			return null;
		}
		
		GrupoRN grupoRN = new GrupoRN();
		if(grupoCadastro.getCodigo() != null && grupoCadastro.getCodigo() != 0)
		{
			Usuario usuarioGrupo = grupoRN.carregarUsuarioDoGrupo(grupoCadastro);
			
			if(usuarioGrupo != null && usuarioGrupo  != this.contextoBean.getUsuarioLogado())
			{
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMessage = new FacesMessage("Só o dono " + usuarioGrupo.getNome() + " pode editar este Questionário.");
				context.addMessage(null, facesMessage);
				grupoCadastro = new Grupo();
				return null;
			}
			
		}
		
		
		this.grupoCadastro.setUsuario(this.contextoBean.getUsuarioLogado());
		grupoRN.salvar(grupoCadastro);
		
		
		this.grupoCadastro = new Grupo();
		this.lista = null;
		return null;
	}
	
	public String excluir()
	{
		
		if(grupoCadastro.getUsuario() == this.contextoBean.getUsuarioLogado())
		{
			GrupoRN grupoRN = new GrupoRN();
			grupoRN.excluir(grupoCadastro);
		}
		
		this.lista = null;
		this.grupoCadastro = new Grupo();
		return null;
	}
	
	public String abrirCadastroDePerguntas()
	{
		if(grupoCadastro.getUsuario() == this.contextoBean.getUsuarioLogado())
		{
			this.contextoBean.setGrupoAtivo(grupoCadastro);
			return "perguntas";
		}else
		{
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Só o dono " + grupoCadastro.getUsuario().getNome() + " pode editar este Questionário.");
			context.addMessage(null, facesMessage);
		}
		
		grupoCadastro = new Grupo();
		return null;
	}
	
	
	public String responderPerguntas()
	{
		this.contextoBean.setGrupoAtivo(grupoCadastro);
		return "responder";
	}
	
	public String abrirRanking()
	{
		this.contextoBean.setGrupoAtivo(grupoCadastro);
		return "ranking";
	}

	public List<Grupo> getLista() {
		
		if(lista == null)
		{
			GrupoRN grupoRN = new GrupoRN();
			this.lista = grupoRN.listar();
		}
		
		return lista;
	}
	
	public Grupo getGrupoCadastro() {
		return grupoCadastro;
	}

	public void setGrupoCadastro(Grupo grupoCadastro) {
		this.grupoCadastro = grupoCadastro;
	}

	public ContextoBean getContextoBean() {
		return contextoBean;
	}

	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}
	
	

}
