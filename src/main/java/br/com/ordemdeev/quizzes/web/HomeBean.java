package br.com.ordemdeev.quizzes.web;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="homeBean")
@RequestScoped
public class HomeBean {
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	
	public String responderQuestionario()
	{
		contextoBean.setNumeroPergunta(0);
		contextoBean.setPerguntasResponder(null);
		return "consultaGrupos";
	}
	
	public String getNomeUsuario()
	{
		return this.contextoBean.getUsuarioLogado().getNome();
	}


	public ContextoBean getContextoBean() {
		return contextoBean;
	}


	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}
	
	
	

}
