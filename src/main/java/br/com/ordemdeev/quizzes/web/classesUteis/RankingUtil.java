package br.com.ordemdeev.quizzes.web.classesUteis;

import br.com.ordemdeev.quizzes.usuario.Usuario;

public class RankingUtil implements Comparable<RankingUtil>{
	
	private Usuario usuario = new Usuario();
	private double somatorio;
	
	public RankingUtil() {}
	
	public RankingUtil(Usuario usuario, double somatorio)
	{
		this.usuario = usuario;
		this.somatorio = somatorio;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public double getSomatorio() {
		return somatorio;
	}
	public void setSomatorio(double somatorio) {
		this.somatorio = somatorio;
	}

	@Override
	public int compareTo(RankingUtil o) {
		
		if(this.somatorio > o.somatorio)
		{
			return -1;
		}
		
		if(this.somatorio < o.somatorio)
		{
			return 1;
		}

		return 0;
	}


	

}
