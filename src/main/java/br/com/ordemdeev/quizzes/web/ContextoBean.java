package br.com.ordemdeev.quizzes.web;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.pergunta.Pergunta;
import br.com.ordemdeev.quizzes.pergunta.PerguntaRN;
import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.usuario.UsuarioRN;


@ManagedBean
@SessionScoped
public class ContextoBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8803396238050792504L;
	
	private Grupo grupoAtivo;
	private Pergunta perguntaAtiva;
	private List<Pergunta> perguntasResponder;
	private int numeroPergunta;
	
	
	public Usuario getUsuarioLogado()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext external = context.getExternalContext();
		String login = external.getRemoteUser();
		if(login != null)
		{
			UsuarioRN usuarioRN = new UsuarioRN();
			return usuarioRN.buscarPorLogin(login);
		}
		return null;
	}
	
	public Pergunta retornaPerguntaLista()
	{
		if(numeroPergunta < getPerguntasResponder().size())
		{
			perguntaAtiva = getPerguntasResponder().get(numeroPergunta);
			return perguntaAtiva;
		}
		
		return null;
	}

	public Grupo getGrupoAtivo() {
		return grupoAtivo;
	}

	public void setGrupoAtivo(Grupo grupoAtivo) {
		this.grupoAtivo = grupoAtivo;
	}

	public Pergunta getPerguntaAtiva() {
		return perguntaAtiva;
	}

	public void setPerguntaAtiva(Pergunta perguntaAtiva) {
		this.perguntaAtiva = perguntaAtiva;
	}

	public List<Pergunta> getPerguntasResponder() {
		
		PerguntaRN perguntaRN = new PerguntaRN();
	    perguntasResponder = perguntaRN.listar(getGrupoAtivo());
		
		
		return perguntasResponder;
	}

	public void setPerguntasResponder(List<Pergunta> perguntasResponder) {
		this.perguntasResponder = perguntasResponder;
	}

	public int getNumeroPergunta() {
		return numeroPergunta;
	}

	public void setNumeroPergunta(int numeroPergunta) {
		this.numeroPergunta = numeroPergunta;
	}
	
	
	

}
