package br.com.ordemdeev.quizzes.web;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import br.com.ordemdeev.quizzes.alternativa.Alternativa;
import br.com.ordemdeev.quizzes.alternativa.AlternativaRN;

@ManagedBean(name = "pontuacaoBean")
@RequestScoped
public class PontuacaoBean {
	
	private List<Alternativa> alternativasSelecionadas = null;
	private double somatorioQuestionario;
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	

	public List<Alternativa> getAlternativasSelecionadas() {
		
		if(alternativasSelecionadas == null)
		{
			AlternativaRN alternativaRN = new AlternativaRN();
			alternativasSelecionadas = alternativaRN.buscarAlternativasUsuario(contextoBean.getUsuarioLogado(), contextoBean.getGrupoAtivo());
		}
		
		return alternativasSelecionadas;
	}

	public void setAlternativasSelecionadas(List<Alternativa> alternativasSelecionadas) {
		this.alternativasSelecionadas = alternativasSelecionadas;
	}

	public double getSomatorioQuestionario() {

		if(somatorioQuestionario == 0.0)
		{
			if(alternativasSelecionadas == null)
			{
				alternativasSelecionadas = this.getAlternativasSelecionadas();
			}
			
			for(int i=0; i < alternativasSelecionadas.size(); i++)
			{
				somatorioQuestionario += alternativasSelecionadas.get(i).getPontos();
			}
		}
		
		return somatorioQuestionario;
	}

	public void setSomatorioQuestionario(double somatorioQuestionario) {
		this.somatorioQuestionario = somatorioQuestionario;
	}

	public ContextoBean getContextoBean() {
		return contextoBean;
	}

	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}
	
	

}
