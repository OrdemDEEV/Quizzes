package br.com.ordemdeev.quizzes.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import br.com.ordemdeev.quizzes.alternativa.Alternativa;
import br.com.ordemdeev.quizzes.alternativa.AlternativaRN;
import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.usuario.UsuarioRN;
import br.com.ordemdeev.quizzes.web.classesUteis.RankingUtil;

@ManagedBean(name = "rankingBean")
@RequestScoped
public class RankingBean {
	
	List<Usuario> usuarios = null;
	List<RankingUtil> listaUsuarios = null;
	
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	
	public List<Usuario> getUsuarios() {
		
		if(usuarios == null)
		{
			UsuarioRN usuarioRN = new UsuarioRN();
			usuarios = usuarioRN.listar();
		}
		
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public ContextoBean getContextoBean() {
		return contextoBean;
	}

	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}

	public List<RankingUtil> getListaUsuarios() {
		
		if(listaUsuarios == null)
		{
			listaUsuarios = new ArrayList<RankingUtil>();
			usuarios = this.getUsuarios();
			List<Alternativa> alternativasSelecionadas;
			AlternativaRN alternativaRN = new AlternativaRN();
			for(int i = 0; i < usuarios.size(); i++)
			{
				alternativasSelecionadas = alternativaRN.buscarAlternativasUsuario(usuarios.get(i), contextoBean.getGrupoAtivo());
				if(alternativasSelecionadas != null)
				{
					double somatorio = 0;
					for(int j=0; j < alternativasSelecionadas.size(); j++)
					{
						somatorio += alternativasSelecionadas.get(j).getPontos();
					}
				
					if(somatorio > 0)
					{
						RankingUtil usuarioParaRanking = new RankingUtil(usuarios.get(i), somatorio);
						listaUsuarios.add(usuarioParaRanking);
					}
				}
			}
		
			if(listaUsuarios != null)
			{
				if(!listaUsuarios.isEmpty())
				{
					Collections.sort(listaUsuarios);
				}
			}
		}
		
		return listaUsuarios;
	}

	public void setListaUsuarios(List<RankingUtil> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	
	
	

}
