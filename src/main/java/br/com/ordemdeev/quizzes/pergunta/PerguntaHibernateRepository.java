package br.com.ordemdeev.quizzes.pergunta;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.util.RepositoryDAOHibernate;

public class PerguntaHibernateRepository extends RepositoryDAOHibernate<Pergunta> implements PerguntaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Pergunta> listarPerguntasGrupo(Grupo grupo) {
		Criteria criteria = super.getSession().createCriteria(Pergunta.class);
		criteria.add(Restrictions.eq("grupo", grupo));
		return criteria.list();
	}
	
	

}
