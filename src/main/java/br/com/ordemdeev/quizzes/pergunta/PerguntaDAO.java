package br.com.ordemdeev.quizzes.pergunta;

import java.util.List;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.util.RepositoryDAO;

public interface PerguntaDAO extends RepositoryDAO<Pergunta>{
	
	public List<Pergunta> listarPerguntasGrupo(Grupo grupo);
	
	
}
