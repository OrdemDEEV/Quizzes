package br.com.ordemdeev.quizzes.pergunta;

import java.util.List;

import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.usuario.UsuarioRN;
import br.com.ordemdeev.quizzes.util.DAOFactory;

public class PerguntaRN {
	
	private PerguntaDAO perguntaDao;
	private double somatorio;
	
	public PerguntaRN() {
		this.perguntaDao = DAOFactory.criarPerguntaDAO();
	}
	
	public List<Pergunta> listar()
	{
		return this.perguntaDao.listar(new Pergunta());
	}
	
	public List<Pergunta> listar(Grupo grupo)
	{
		return this.perguntaDao.listarPerguntasGrupo(grupo);
	}
	
	public Pergunta carregar(Integer codigo)
	{
		return this.perguntaDao.carregar(new Pergunta(),codigo);
	}
	
	public void salvar(Pergunta pergunta)
	{
		this.perguntaDao.salvar(pergunta);
	}
	
	public void excluir(Pergunta pergunta)
	{
		UsuarioRN usuarioRN = new UsuarioRN();
		List<Usuario> usuarios = usuarioRN.listar();
		
		//esse for é para remover todas as questões todas as respostas das perguntas feita nesse grupo
		for(int i=0;i<usuarios.size(); i++)
		{
			
		   usuarioRN.removerPerguntasRespondidas(usuarios.get(i), pergunta.getAlternativas() );
			
		}
		
		pergunta = carregar(pergunta.getCodigo());
		this.perguntaDao.excluir(pergunta);
	}

	public double getSomatorio() {
		return somatorio;
	}

	public void setSomatorio(double somatorio) {
		this.somatorio = somatorio;
	}
	
	

}
