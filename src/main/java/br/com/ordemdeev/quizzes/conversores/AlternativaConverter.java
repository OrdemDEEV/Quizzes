package br.com.ordemdeev.quizzes.conversores;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import br.com.ordemdeev.quizzes.alternativa.Alternativa;
import br.com.ordemdeev.quizzes.alternativa.AlternativaRN;

@FacesConverter(value = "alternativaConverter", forClass = Alternativa.class)
public class AlternativaConverter implements javax.faces.convert.Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value != null && value.trim().length() > 0) {
            try {
                AlternativaRN alternativaRN = new AlternativaRN();
                return alternativaRN.carregar(Integer.parseInt(value));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO de Conversão", "Alternativa Invalida"));
            }
        }
        else {
            return null;
        }
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		 if (value != null && (value instanceof Alternativa)) {
	            return String.valueOf(((Alternativa) value).getCodigo());
	        }
		return null;
	}

}
