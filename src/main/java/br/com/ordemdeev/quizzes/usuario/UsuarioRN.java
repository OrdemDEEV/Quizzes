package br.com.ordemdeev.quizzes.usuario;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import br.com.ordemdeev.quizzes.alternativa.Alternativa;
import br.com.ordemdeev.quizzes.grupo.Grupo;
import br.com.ordemdeev.quizzes.grupo.GrupoRN;
import br.com.ordemdeev.quizzes.util.DAOFactory;

public class UsuarioRN {
	private UsuarioDAO usuarioDAO;

	public UsuarioRN() {
		this.usuarioDAO = DAOFactory.criarUsuarioDAO();
	}

	public Usuario carregar(Integer codigo) {
		return this.usuarioDAO.carregar(new Usuario(), codigo);
	}
	
	public Usuario carregarUsuarioDoGrupo(Grupo grupo)
	{
		GrupoRN grupoRN = new GrupoRN();
		grupo = grupoRN.carregar(grupo.getCodigo());
		return grupo.getUsuario();
	}

	public Usuario buscarPorLogin(String login) {
		return this.usuarioDAO.buscarPorLogin(login);
	}

	public void salvar(Usuario usuario) {
		Integer codigo = usuario.getCodigo();
		if (codigo == null || codigo == 0) {
			//sempre que for criado um usuario, ele vai ter o papel de usuario nas permssões
			usuario.getPermissao().add("ROLE_USUARIO");
			this.usuarioDAO.salvar(usuario);
		} else {
			this.usuarioDAO.atualizar(usuario);
		}
	}

	public void excluir(Usuario usuario) {
		this.usuarioDAO.excluir(usuario);
	}

	public List<Usuario> listar() {
		return this.usuarioDAO.listar(new Usuario());
	}
	
	public void removerPerguntasRespondidas(Usuario usuario, List<Alternativa> lista)
	{
		
		if(lista != null && usuario.getRespostas() != null)
		{
			for(int i = 0; i < lista.size(); i++)
			{
				 Iterator<Alternativa> alternativaAsIterator = usuario.getRespostas().iterator();
				 while (alternativaAsIterator.hasNext())
				 {
					 
					 Alternativa it =  alternativaAsIterator.next();
					 if(lista.get(i).getCodigo().equals(it.getCodigo()))
					 {
						alternativaAsIterator.remove();
						break;
					 }
	             }
			}
			
			this.salvar(usuario);
		
		 }
	 }
	
	public void removerPerguntasRespondidas(Usuario usuario, Set<Alternativa> lista)
	{
		//precifo fazer funfar aqui, ai vai conseguir deletar tudo de boas
		
		if(lista != null && usuario.getRespostas() != null)
		{
			Iterator<Alternativa> alternativaPerguntaIterator = lista.iterator();
			while(alternativaPerguntaIterator.hasNext())
			{
				 Iterator<Alternativa> respostasUsuarioIt = usuario.getRespostas().iterator();
				 Alternativa alternativaAtualIt = alternativaPerguntaIterator.next();
				 while (respostasUsuarioIt.hasNext())
				 {
					 Alternativa escolhaUsuario =  respostasUsuarioIt.next();
					 if(alternativaAtualIt.getCodigo().equals(escolhaUsuario.getCodigo()))
					 {
						respostasUsuarioIt.remove();
						break;
					 }
	             }
			}
			
			
			this.salvar(usuario);
		
		 }
	 }

}
