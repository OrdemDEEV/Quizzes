package br.com.ordemdeev.quizzes.usuario;

import org.hibernate.Query;

import br.com.ordemdeev.quizzes.util.RepositoryDAOHibernate;

public class UsuarioHibernateRepository extends RepositoryDAOHibernate<Usuario> implements UsuarioDAO {

	public Usuario buscarPorLogin(String login) {
		String hql = "select u from Usuario u where u.login = :login";
		Query consulta = super.getSession().createQuery(hql);
		consulta.setString("login", login);
		return (Usuario) consulta.uniqueResult();
	}

	@Override
	public void atualizar(Usuario usuario) {
		if (usuario.getPermissao() == null || usuario.getPermissao().size() == 0) {
			Usuario usuarioPermissao = super.carregar( new Usuario(), usuario.getCodigo());
			usuario.setPermissao(usuarioPermissao.getPermissao());
			super.getSession().evict(usuarioPermissao);
		}
		super.getSession().update(usuario);
		
	}

	/*@SuppressWarnings("unchecked")
	@Override
	public List<Integer> buscarCodRespostasGrupo(Integer usuario, Integer grupo) {
		
		String hql = "select a.COD_ALTERNATIVA from RESPOSTAS a ";
		hql += "left outer join ALTERNATIVA b on a.COD_ALTERNATIVA = b.COD_ALTERNATIVA ";
		hql += "left outer join PERGUNTA c on b.pergunta_COD_PERGUNTA = c.COD_PERGUNTA ";
		hql += "left outer join GRUPO d on d.COD_GRUPO = grupo_COD_GRUPO ";
		hql += "where d.COD_GRUPO = :cod_grupo and a.COD_USUARIO = :cod_usuario";
		Query consulta = super.getSession().createQuery(hql);
		consulta.setInteger("cod_grupo", grupo);
		consulta.setInteger("cod_usuario", usuario);
		return  consulta.list();
		
	}*/


}
