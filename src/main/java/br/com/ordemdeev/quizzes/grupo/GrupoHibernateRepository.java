package br.com.ordemdeev.quizzes.grupo;


import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.util.RepositoryDAOHibernate;

public class GrupoHibernateRepository extends RepositoryDAOHibernate<Grupo> implements GrupoDao {

	@Override
	public Usuario carregarUsuarioGrupo(Grupo grupo) {
		Grupo grupoComUsuario = this.carregar(new Grupo(), grupo.getCodigo());
		Usuario usuarioCarregado = grupoComUsuario.getUsuario();
		this.getSession().evict(grupoComUsuario);
		return usuarioCarregado;
	}

	

}
