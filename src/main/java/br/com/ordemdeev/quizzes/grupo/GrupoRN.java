package br.com.ordemdeev.quizzes.grupo;

import java.util.Iterator;
import java.util.List;

import br.com.ordemdeev.quizzes.pergunta.Pergunta;
import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.usuario.UsuarioRN;
import br.com.ordemdeev.quizzes.util.DAOFactory;

public class GrupoRN {
	
	private GrupoDao grupoDao;
	
	public GrupoRN() {
		this.grupoDao = DAOFactory.criarGrupoDAO();
	}
	
	public List<Grupo> listar()
	{
		return this.grupoDao.listar(new Grupo());
	}
	
	public Grupo carregar(Integer codigo)
	{
		return this.grupoDao.carregar(new Grupo(),codigo);
		
	}
	
	public Usuario carregarUsuarioDoGrupo(Grupo grupo)
	{
		return this.grupoDao.carregarUsuarioGrupo(grupo);
	}
	
	public void salvar(Grupo grupo)
	{
		
		this.grupoDao.salvar(grupo);
		
	}
	
	
	public void excluir(Grupo grupo)
	{
		
		UsuarioRN usuarioRN = new UsuarioRN();
		List<Usuario> usuarios = usuarioRN.listar();
		
		//esse for é para remover todas as questões todas as respostas das perguntas feita nesse grupo
		for(int i=0;i<usuarios.size(); i++)
		{
			Iterator<Pergunta> perguntasIT = grupo.getPerguntas().iterator();
			while(perguntasIT.hasNext())
			{
				usuarioRN.removerPerguntasRespondidas(usuarios.get(i), perguntasIT.next().getAlternativas() );
			}
		}
		
		grupo = carregar(grupo.getCodigo());
		this.grupoDao.excluir(grupo);
	}

}
