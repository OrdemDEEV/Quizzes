package br.com.ordemdeev.quizzes.grupo;


import br.com.ordemdeev.quizzes.usuario.Usuario;
import br.com.ordemdeev.quizzes.util.RepositoryDAO;

public interface GrupoDao extends RepositoryDAO<Grupo>{
	
	public Usuario carregarUsuarioGrupo(Grupo grupo);
	
}
