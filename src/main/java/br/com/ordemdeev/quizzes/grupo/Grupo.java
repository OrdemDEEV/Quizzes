package br.com.ordemdeev.quizzes.grupo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import br.com.ordemdeev.quizzes.pergunta.Pergunta;
import br.com.ordemdeev.quizzes.usuario.Usuario;

@Entity
@Table(name = "GRUPO")
public class Grupo implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 500225433722638127L;
	@Id
	@GeneratedValue
	@Column(name = "COD_GRUPO")
	private Integer codigo;
	
	private String titulo;
	private String descricao;
	
	//esse mappedBy é o nome do campo em Pergunta que esta mapeando
	@OneToMany(mappedBy = "grupo", targetEntity = Pergunta.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Pergunta> perguntas;
	
	@ManyToOne()
	@OnDelete(action=OnDeleteAction.CASCADE)
	@JoinColumn(nullable = false, updatable = false)
	private Usuario usuario;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Pergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(Set<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupo other = (Grupo) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	
	
	
	
	
	
	
	

}
